$(document).ready(function() {
	f_name = "Michael"
	l_name = "Nader"

	address = "48StreetName"
	city = "Melbourne"
	zip = "3000"

	card_num = "4003600000000006"
	expiry_date = "06/2022"
	csv = "465"

	refresh_data();
});

function refresh_data(){
	$('.f_name').html(f_name);
	$('.l_name').html(l_name);
	$('.address').html(address);
	$('.city').html(city);
	$('.zip').html(zip);
	$('.card_num').html(card_num);
	$('.expiry_date').html(expiry_date);
	$('.csv').html(csv);
	$('.button').html('<button type="button" class="edit">Edit</button>');
}

$(document).on('click', '.edit', function(){
	$('.f_name').html('<input type="text" name="f_name" placeholder='+ f_name +'></input>');
	$('.l_name').html('<input type="text" name="l_name" placeholder='+ l_name +'></input>');
	$('.address').html('<input type="text" name="address" placeholder='+ address +'></input>');
	$('.city').html('<input type="text" name="city" placeholder='+ city +'></input>');
	$('.zip').html('<input type="text" name="zip" placeholder='+ zip +'></input>');
	$('.card_num').html('<input type="text" name="card_num" placeholder='+ card_num +'></input>');
	$('.expiry_date').html('<input type="text" name="expiry_date" placeholder='+ expiry_date +'></input>');
	$('.csv').html('<input type="text" name="csv" placeholder='+ csv +'></input>');
	$('.button').html('<button type="button" class="submit">Submit Changes</button>');
	$('.button').append('&nbsp; <button type="button" class="back">Cancel</button>');
});

$(document).on('click', '.back', function(){
	refresh_data();
});

$(document).on('click', '.submit', function(){
	//Validate
	error = "";
	temp_f_name = $("input[name=f_name]").val();
	temp_l_name = $("input[name=l_name]").val();
	temp_address = $("input[name=address]").val();
	temp_city = $("input[name=city]").val();
	temp_zip = $("input[name=zip]").val();
	temp_card_num = $("input[name=card_num]").val();
	temp_expiry_date = $("input[name=expiry_date]").val();
	temp_csv = $("input[name=csv]").val();
	
	if(temp_f_name == null || temp_f_name == "")
		error += "First name can't be blank \n";
	
	if(temp_l_name == null || temp_l_name == "")
		error += "Last name can't be blank \n";
	
	if(temp_address == null || temp_address == "" || !has_num(temp_address))
		error += "Address name can't be blank and must contain a number \n";
	
	if(temp_city == null || temp_city == "")
		error += "City can't be blank \n";
	
	if(temp_zip == null || temp_zip == "" || temp_zip.length != 4|| !is_numeric(temp_zip))
		error += "Zip must contain 4 digits \n";
	
	if(temp_card_num == null || temp_card_num == "" || temp_card_num.length <15 || temp_card_num.length >19|| !is_numeric(temp_card_num))
		error += "Card number must contain between 15 and 19 digits \n";
	
	if(temp_expiry_date == null || temp_expiry_date == "")
		error += "Expiry date can't be blank \n";
	
	if(temp_csv == null || temp_csv == "" || temp_csv.length !=3|| !is_numeric(temp_csv))
		error += "CSV number must contain 3 digits \n";
	
	if (error != ""){
		alert(error);
		return;
	}
	
	f_name = temp_f_name
	l_name = temp_l_name
	address = temp_address
	city = temp_city
	zip = temp_zip
	card_num = temp_card_num
	expiry_date = temp_expiry_date
	csv = temp_csv
	
	refresh_data();
});

function has_num(s) {
  return /\d/.test(s);
}

function is_numeric(num)
{
    return (num - 0) == num && (''+num).trim().length > 0;
}